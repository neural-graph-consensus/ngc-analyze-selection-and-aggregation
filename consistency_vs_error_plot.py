#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from natsort import natsorted
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from lovely_numpy import lo
from numpy.typing import NDArray
from simple_caching import cache_fn
from simple_caching.storage import NpyFS
import hashlib

from main import get_output_nodes

def get_best_performance_edge_dir(dump_dir: Path, it1_results_dir: Path, node: str) -> Path:
    """given a dump dir, a node and the path to the csv results file, return the path to the node's dump data"""
    csv_file_path = it1_results_dir / f"{node}_results_edges_test.csv"
    assert csv_file_path.exists(), csv_file_path
    df = pd.read_csv(csv_file_path).sort_values("l2")
    best_performing_edge = df[df["edges"].str.startswith("Single ")]["edges"].iloc[0]
    best_performing_edge_dir = dump_dir / node / "0" / best_performing_edge / "npy"
    assert best_performing_edge_dir.exists(), best_performing_edge_dir
    return best_performing_edge_dir

@cache_fn(NpyFS, key_encode_fn=lambda res_dir: hashlib.md5(str(res_dir.absolute()).encode("utf-8")).hexdigest())
def read_from_dir(res_dir: Path) -> NDArray["n,h,w,c"]:
    assert res_dir.exists(), res_dir
    files = natsorted([x for x in res_dir.glob("*.npz")], key=lambda p: p.name)
    assert len(files) > 0, res_dir
    data = np.array([np.load(x)["arr_0"] for x in files])
    return data

def cons(data: NDArray["k,h,w,c"]) -> NDArray["h,w,c"]:
    """consistency for 1 volumne, cented in 1 data point. For start/end time points, the volume can be even too"""
    res = np.std(data, axis=0)
    return res

def compute_consistency(y_vol: NDArray["n,h,w,c"], k: int) -> NDArray["n,h,w,c"]:
    """Compute consistency between -k/+k neighboring frames"""
    print(f"Computing consistency (k={k})")
    timespan = y_vol.shape[0]
    consistency_array = []
    for index in range(timespan):
        start_idx = max(0, index - k)
        end_idx = min(timespan, index + k + 1)
        cons_map = cons(y_vol[start_idx: end_idx])
        consistency_array.append(cons_map)
    assert len(consistency_array) == timespan
    res = np.array(consistency_array)
    return res

def l2(y: NDArray["h,w,c"], gt: NDArray["h,w,c"]) -> NDArray["h,w,c"]:
    """l2 for 1 item -- returns a map with nans on invalid pixels"""
    assert len(y.shape) == 3 and len(gt.shape) == 3
    y_orig_shape = y.shape
    y = y.flatten().astype(float)
    gt = gt.flatten().astype(float)
    mask = gt != 0
    result = np.full_like(y, np.nan)
    result[mask] = (y[mask] - gt[mask]) ** 2
    result = result.reshape(y_orig_shape)
    return result

def compute_l2(y_data: NDArray["n,h,w,c"], gt_data: NDArray["n,h,w,c"]) -> NDArray["n,h,w,c"]:
    """Appies the metric_fn for the (y, gt) data. Returns the per-map score."""
    print("Computing L2")
    assert len(y_data) == len(gt_data)
    res = np.zeros_like(y_data, dtype=np.float32)
    for i, (y, gt) in enumerate(zip(y_data, gt_data)):
        res[i] = l2(y, gt)
    print(f"Computed L2 for {len(y_data)} maps. Invalid pixels: {np.isnan(res).sum() / np.prod(res.shape) * 100:.2f}%")
    return res

def _get_freqs(x: NDArray["n"], y: NDArray["n"], n_bins: int) -> (NDArray["n_bins,b_nins"], (np.ndarray, np.ndarray)):
    assert len(x) == len(y) and len(x.shape) == 1, (x.shape, y.shape)
    x_ls = np.linspace(0, x.max(), n_bins)
    y_ls = np.linspace(0, y.max(), n_bins)
    x_digit = np.digitize(x, x_ls) - 1
    y_digit = np.digitize(y, y_ls) - 1
    uniqs, freqs = np.unique(np.stack([x_digit, y_digit], axis=1), return_counts=True, axis=0)
    res = np.zeros((n_bins, n_bins), dtype=np.int64)
    res[uniqs[:, 0], uniqs[:, 1]] = freqs
    return res, (x_ls, y_ls)

def plot_err_vs_cons(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"], out_path: Path,
                     plot_n_sample: int = None, plot_title: str = None):
    """Plot the error vs consistency on the valid pixels only"""
    l2_all = l2_all.flatten()
    cons_all = cons_all.flatten()
    mask = np.isnan(l2_all)
    l2_valid = l2_all[~mask]
    cons_valid = cons_all[~mask]

    if plot_n_sample is not None:
        idx = np.random.choice(len(l2_valid), plot_n_sample, replace=False)
        l2_valid = l2_valid[idx]
        cons_valid = cons_valid[idx]

    cons_valid = np.clip(cons_valid, 0, np.percentile(cons_valid, 99))
    l2_valid = np.clip(l2_valid, 0, np.percentile(l2_valid, 99))
    freqs, bins = _get_freqs(cons_valid, l2_valid, n_bins=10)

    x_ls = np.linspace(0, cons_valid.max(), len(cons_valid))
    y_avg_per_bin = np.zeros_like(l2_valid)
    bins = bins[0]
    for i in range(len(bins) - 1):
        l, r = len(cons_valid) * i // len(bins), len(cons_valid) * (i + 1) // len(bins) 
        scores = l2_valid[(cons_valid >= bins[i]) & (cons_valid < bins[i+1])]
        if len(scores) == 0:
            breakpoint()
        y_avg_per_bin[l:r] = scores.mean()

    plt.clf()
    plt.plot(cons_valid, l2_valid, "o")
    plt.plot(x_ls, l2_valid.mean() * np.ones_like(l2_valid), "-", label="mean")
    plt.plot(x_ls, y_avg_per_bin, "-", label="mean_per_bin")
    if plot_title is not None:
        plt.title(plot_title)
    plt.xlabel("Standard deviation of neighboring months")
    plt.ylabel("L2 error (on valid pixels)")
    plt.savefig(out_path, bbox_inches="tight")

    print(f"Saved at '{out_path}'")

def plot_err_vs_cons_3d_bar(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"], out_path: Path,
                            n_bins: int, plot_n_sample: int = None, plot_title: str = None):
    """Plot the error vs consistency on the valid pixels only"""
    l2_all = l2_all.flatten()
    cons_all = cons_all.flatten()
    mask = np.isnan(l2_all)
    l2_valid = l2_all[~mask]
    cons_valid = cons_all[~mask]

    if plot_n_sample is not None:
        idx = np.random.choice(len(l2_valid), plot_n_sample, replace=False)
        l2_valid = l2_valid[idx]
        cons_valid = cons_valid[idx]

    cons_valid = np.clip(cons_valid, 0, np.percentile(cons_valid, 99))
    l2_valid = np.clip(l2_valid, 0, np.percentile(l2_valid, 99))
    freqs, bins = _get_freqs(cons_valid, l2_valid, n_bins=n_bins)
    grid = np.meshgrid(np.arange(n_bins), np.arange(n_bins))
    xx = grid[0].ravel()
    yy = grid[1].ravel()
    bottom = np.zeros_like(xx)
    top = np.log(freqs.ravel())

    plt.clf()
    fig = plt.figure()
    ax = plt.axes(projection="3d")
    ax.bar3d(xx, yy, bottom, 1, 1, top, shade=True)
    ax.set_xlabel("Std of neighboring months")
    ax.set_ylabel("L2 error * 1000")
    ax.set_zlabel("Log(Frequency)")
    ax.set_xticklabels(np.linspace(0, cons_valid.max(), n_bins).round(3))
    ax.set_yticklabels((np.linspace(0, l2_valid.max(), n_bins) * 1000).round(2))
    ax.set_title(plot_title)
    plt.savefig(out_path, bbox_inches="tight", pad_inches=0.2)

def plot_err_vs_cons_3d_hist(l2_all: NDArray["n,h,w,c"], cons_all: NDArray["n,h,w,c"], out_path: Path,
                             n_bins: int, plot_n_sample: int = None, plot_title: str = None):
    """Plot the error vs consistency on the valid pixels only"""
    l2_all = l2_all.flatten()
    cons_all = cons_all.flatten()
    mask = np.isnan(l2_all)
    l2_valid = l2_all[~mask]
    cons_valid = cons_all[~mask]
    cons_ls = np.linspace(0, cons_valid.max(), n_bins)
    l2_ls = np.linspace(0, l2_valid.max(), n_bins)

    if plot_n_sample is not None:
        idx = np.random.choice(len(l2_valid), plot_n_sample, replace=False)
        l2_valid = l2_valid[idx]
        cons_valid = cons_valid[idx]

    cons_valid = np.clip(cons_valid, 0, np.percentile(cons_valid, 99))
    l2_valid = np.clip(l2_valid, 0, np.percentile(l2_valid, 99))
    freqs, bins = _get_freqs(cons_valid, l2_valid, n_bins=n_bins)

    a, b = 0, 3
    A, B = freqs[a], freqs[b]
    A, B = A / A.sum(), B / B.sum()

    bars = []
    plt.clf()
    bars.append(plt.bar(np.arange(n_bins) - 0.4, A, width=0.4))
    bars.append(plt.bar(np.arange(n_bins), B, width=0.4))
    plt.legend(bars, [f"std∈{bins[0][a]:.2f}-{bins[0][a+1]:.2f}", f"std∈{bins[0][b]:.2f}-{bins[0][b+1]:.2f}"])
    plt.xticks(np.arange(n_bins), bins[1].round(3))
    plt.xlabel("L2 error")
    plt.ylabel("Frequency")
    plt.title(f"{plot_title}. Totals: {freqs[a].sum()} - {freqs[b].sum()}. Max std: {cons_valid.max():.3f}")
    plt.savefig(out_path)

def get_args():
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("dump_dir", type=Path)
    parser.add_argument("--it1_results_dir", type=Path, required=True)
    parser.add_argument("--node", required=True, help="Which node's output to process")
    parser.add_argument("--cons_k", type=int, default=3, help="How many neighboring frames to use for consistency")
    parser.add_argument("--plot_out_path", "-o", type=lambda p: Path(p).absolute(), required=True)
    parser.add_argument("--plot_n_sample", type=int, help="How many data points to sample for the final plot")
    parser.add_argument("--plot_title")
    parser.add_argument("--overwrite", action="store_true")
    args =  parser.parse_args()
    assert args.node in get_output_nodes(args.dump_dir)
    assert args.dump_dir.exists(), args.dump_dir
    assert args.it1_results_dir.exists(), args.it1_results_dir
    assert not args.plot_out_path.exists() or args.overwrite, f"Output path already exists: '{args.plot_out_path}'"
    return args

def main():
    """main fn"""
    args = get_args()
    best_performing_edge_dir = get_best_performance_edge_dir(args.dump_dir, args.it1_results_dir, args.node)
    gt_dir = args.it1_results_dir / args.node / "gt/state/npy"
    assert gt_dir.exists(), gt_dir

    x_data = read_from_dir(best_performing_edge_dir)
    gt_data = read_from_dir(gt_dir)

    l2_all = compute_l2(x_data, gt_data)
    cons_all = compute_consistency(x_data, k=args.cons_k)

    plot_err_vs_cons(l2_all, cons_all, out_path=args.plot_out_path,
                     plot_n_sample=args.plot_n_sample, plot_title=args.plot_title)
    bar_path = args.plot_out_path.with_name(args.plot_out_path.stem + "_3d_bar").with_suffix(".png")
    plot_err_vs_cons_3d_bar(l2_all, cons_all, out_path=bar_path, plot_n_sample=args.plot_n_sample,
                             n_bins=100, plot_title=args.plot_title)
    hist_path = args.plot_out_path.with_name(args.plot_out_path.stem + "_hist").with_suffix(".png")
    plot_err_vs_cons_3d_hist(l2_all, cons_all, out_path=hist_path, n_bins=10, plot_n_sample=args.plot_n_sample,
                             plot_title=args.plot_title)

if __name__ == "__main__":
    main()
