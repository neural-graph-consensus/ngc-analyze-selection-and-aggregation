# NGC Analysis for Selection & Aggregation

Given a dump directory via [the dump script](https://gitlab.com/neural-graph-consensus/ngc-single-links/-/blob/master/main_dump_predictions.py), we use this repository to make an anlysis of the outputs and various selection (keep top k) and aggregation (mean, median, weighted mean) options in order to improve the baseline (that was exported). We also have access to the GT of each output node.

